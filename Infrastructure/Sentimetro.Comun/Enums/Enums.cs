﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sentimetro.Comun.Enums
{
    public enum eNivelFelicidad
    {
        Enfadado = 0,
        Normal = 1,
        Feliz = 2
    }
    public enum eNivelCarga
    {
        Poco = 0,
        Normal = 1,
        Mucho = 2
    }
}
