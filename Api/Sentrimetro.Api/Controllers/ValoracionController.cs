﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sentimetro.Entities;
using Sentimetro.Service;
using Sentrimetro.Api.Controllers.Core;

namespace Sentrimetro.Api.Controllers
{
    public class ValoracionController : BaseController
    {
        private readonly ValoracionService _service;

        public ValoracionController()
        {
            this._service = new ValoracionService();
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var result = await _service.GetAsync();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DateTime? from)
        {
            var result = await _service.GetAsync(from);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DateTime? from, DateTime? to)
        {
            var result = await _service.GetAsync(from, to);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(Valoracion valoracion)
        {
            await _service.SaveAsync(valoracion);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
