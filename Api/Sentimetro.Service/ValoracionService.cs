﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sentimetro.Comun.Filters;
using Sentimetro.Domain;
using Sentimetro.Entities;

namespace Sentimetro.Service
{
    public class ValoracionService
    {
        private readonly ValoracionDomain dominio;


        public ValoracionService()
        {
            this.dominio = new ValoracionDomain();
        }

        public async Task<IEnumerable<Valoracion>> GetAsync(DateTime? from = null, DateTime? to = null, bool isTest = true)
        {
            var filter = new FromToDateFilter
            {
                From = from,
                To = to
            };
            return await this.dominio.GetAsync(filter, isTest);
        }

        public async Task<List<Valoracion>> GetAllAsync()
        {
            return await dominio.GetAllAsync();
        }

        public async Task SaveAsync(Valoracion valoracion)
        {
            await dominio.SaveAsync(valoracion);
        }

       
    }
}
