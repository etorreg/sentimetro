﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using Sentimetro.Comun.Constants;
using Sentimetro.Comun.Filters;
using Sentimetro.Entities;
using Sentimetro.Repository.Core;

namespace Sentimetro.Repository
{
    public class ValoracionRepository : BaseRepository<Valoracion>
    {
        public ValoracionRepository()
            : base()
        {
            this.collectionName = CollectionsNameContants.Valoraciones;
        }

        public override async Task SaveAsync(Valoracion valoracion)
        {
            valoracion.TimeStamp = DateTime.UtcNow;
            await base.SaveAsync(valoracion);
        }

        public async Task<IEnumerable<Valoracion>> GetAsync(FromToDateFilter fromToDate, bool isTest)
        {

            var filterBuilder = Builders<Valoracion>.Filter;
            FilterDefinition<Valoracion> filter;

            if (fromToDate.From != null && fromToDate.To != null) filter = filterBuilder.Gte("TimeStamp", fromToDate.From) & filterBuilder.Lte("TimeStamp", fromToDate.To);
            else if (fromToDate.From != null) filter = filterBuilder.Gte("TimeStamp", fromToDate.From);
            else if (fromToDate.To != null) filter = filterBuilder.Lte("TimeStamp", fromToDate.To);
            else filter = filterBuilder.Empty;

            return await this.Collection.FindAsync(filter & filterBuilder.Eq("Test", isTest)).Result.ToListAsync();
        }
    }
}
