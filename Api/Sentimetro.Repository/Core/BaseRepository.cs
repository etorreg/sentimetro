﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Sentimetro.Repository.Core
{
    public class BaseRepository<T>
    {
        private Context.Context Context;
        protected IMongoDatabase Database { get { return this.Context._database; } }

        private IMongoCollection<T> _collection;
        protected string collectionName;
        protected IMongoCollection<T> Collection
        {
            get
            {
                if (this._collection == null) this._collection = this.Database.GetCollection<T>(this.collectionName);
                return this._collection;
            }
        }

        public BaseRepository()
        {
            Context = new Context.Context();
        }


        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await this.Collection.FindAsync(_ => true).Result.ToListAsync();
            return result;
        }

        public virtual async Task SaveAsync(T valoracion)
        {
            await this.Collection.InsertOneAsync(valoracion);
        }
    }
}
