using Sentimetro.App.Droid.Renderers;
using Sentimetro.App.Views.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace Sentimetro.App.Droid.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control == null) return;
            Control.SetBackgroundColor(Color.Red);
            Control.SetTextColor(Color.Pink);
        }
    }
}