﻿using System.Windows.Input;
using Xamarin.Forms;

namespace Sentimetro.App.Views.Controls
{
    public partial class CustomButtonView : ContentView
    {
        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty,value);}
        }
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text),typeof(string),typeof(CustomButtonView));

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly BindableProperty IsSelectedProperty =
            BindableProperty.Create(nameof(IsSelected), typeof(bool), typeof(CustomButtonView), false, BindingMode.TwoWay);

        public ICommand Command
        {
            get { return (ICommand) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value);}
        }
        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof (ICommand), typeof (CustomButtonView), null,
                BindingMode.TwoWay);

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
        public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(CustomButtonView), null,
                BindingMode.TwoWay);


        public CustomButtonView()
        {
            InitializeComponent();
        }
    }
}
