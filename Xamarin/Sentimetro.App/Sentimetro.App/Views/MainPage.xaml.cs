﻿using GalaSoft.MvvmLight.Messaging;
using Xamarin.Forms;

namespace Sentimetro.App.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage<string>>(this, x =>
            {
                DisplayAlert(x.Notification == "OK" ? "Good work" : "Ooops", x.Content, "Ok");
            });
        }
    }
}
