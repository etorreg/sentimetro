﻿namespace Sentimetro.App.Core
{
    public interface INavigationService
    {
        void Configure<T, TV>(string identifier = null) where T : class where TV : class;
        void GoBack();
        void NavigateTo<T>() where T : class;
        void NavigateTo<T>(object parameter, string identifier = null) where T : class;
    }
}
