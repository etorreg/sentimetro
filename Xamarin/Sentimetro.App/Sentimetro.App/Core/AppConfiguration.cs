﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Sentimetro.App.ViewModels;
using Sentimetro.App.Views;
using Xamarin.Forms;

namespace Sentimetro.App.Core
{
    public class AppConfiguration
    {
        private INavigationService _navigation;
        public NavigationPage FirstPage { get; private set; }

        public AppConfiguration()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            RegisterViewModels();
            RegisterServices();
            RegisterNavigation();
        }

        private void RegisterViewModels()
        {
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<SecondViewModel>();
        }


        private void RegisterServices()
        {

        }

        private void RegisterNavigation()
        {
            FirstPage = new NavigationPage(new MainPage());
            _navigation = new NavigationService(ServiceLocator.Current, FirstPage);
            SimpleIoc.Default.Register(() => _navigation);
            FirstPage.BindingContext = ServiceLocator.Current.GetInstance<MainViewModel>();

            _navigation.Configure<MainViewModel, MainPage>();
            _navigation.Configure<SecondViewModel, SecondPage>();
        }




    }
}
