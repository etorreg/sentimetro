﻿window.app = window.app || {};
window.app.helpers = window.app.helpers || {};

window.app.helpers.ajaxHelper = (function () {
    var xhr = function (verb, url, options, successcallback, errorcallback, allwayscallback) {
        var req = new XMLHttpRequest();

        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    successcallback(req.responseText);
                } else {
                    if (errorcallback) {
                        errorcallback(req);
                    }
                }
                if (allwayscallback) {
                    allwayscallback(req);
                }
            }
        };
        req.open(verb, url);
        if (options) {
            if (options.headers) {
                for (var counter = 0; counter < options.headers.length; counter++) {
                    var header = options.headers[counter];
                    req.setRequestHeader(header.dataType, header.value);
                }
            }
            if (options.object) {
                req.send(options.object);
                return;
            }

        }
        req.send();
    }
    var ajaxHelper = {
        get: function (url, options, successcallback, errorcallback, allwayscallback) {
            xhr('GET', url, options, successcallback, errorcallback, allwayscallback);
        },
        post: function (url, options, successcallback, errorcallback, allwayscallback) {
            xhr('POST', url, options, successcallback, errorcallback, allwayscallback);
        },
        put: function (url, options, parameters, successcallback, errorcallback, allwayscallback) {
            xhr('PUT', url, options, successcallback, errorcallback, allwayscallback);
        },
        del: function (url, options, parameters, successcallback, errorcallback, allwayscallback) {
            xhr('DEL', url, options, successcallback, errorcallback, allwayscallback);
        }
    };

    return ajaxHelper;
})();

/*
AjaxHelper object will use XMLHttpRequest to make requests to the server.
It exposes the following verbs:
- get
- post
- put
- del
Every exposed method take the following arguments:
- url: url to be requested.
- options: at this time it will manage only a property headers that is an array that let 
           you indicate the different headers to be send to the server in the following format:
           {dataType : 'Content-type', value:'application/x-www-form-urlencoded'}
- successcallback:  function to be executed if the request was executed successfully
- errorcallback:  function to be executed if the request has an error.
- alwayscallback:  function to be executed always
Example of use:
window.app.common.helpers.ajaxHelper.get('/api/users/me',null, loadItemCallback);

*/