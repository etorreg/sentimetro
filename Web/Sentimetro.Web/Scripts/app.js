﻿(function () {
    var carga = {
        poca: { key: "carga_poca", value: 0 },
        normal: { key: "carga_normal", value: 1 },
        mucha: { key: "carga_mucha", value: 2 },
    },
        feliz = {
            enfadado: { key: "cara_enfadado", value: 0 },
            normal: { key: "cara_normal", value: 1 },
            feliz: { key: "cara_feliz", value: 2 },
        },
    send = "enviar",
    self = null,
    ajaxHelper = null,
    urlValoracion = null;

    function App() {
        self = this;
        self.felicidad = null;
        self.carga = null;

        ajaxHelper = window.app.helpers.ajaxHelper;
        urlValoracion = window.app.urls.apiValoracion;
        configureEvents();
    };

    function configureEvents() {
        addClickCarga(carga.poca);
        addClickCarga(carga.normal);
        addClickCarga(carga.mucha);

        addClickFelicidad(feliz.feliz);
        addClickFelicidad(feliz.normal);
        addClickFelicidad(feliz.enfadado);

        document.getElementById(send).addEventListener('click', sendData);
    };

    function sendData() {
        if (isValid()) {
            var options = {
                object: JSON.stringify(castToServer()),
                headers: [{ dataType: 'Content-Type', value: 'application/json' }]
            };

            ajaxHelper.post(urlValoracion, options, function () {
                self.felicidad = null;
                self.carga = null;
                clearSelecteds('');
                alert("ok :)");
            }, function () { alert("error :("); })
        }
    };

    function isValid() {
        var result = self.felicidad != null && self.carga != null;

        if (!result) alert("La carga o la felicidad no estan seleccionadas");

        return result;
    };

    function castToServer() {
        var that = {
            NivelFelicidad: self.felicidad,
            NivelCarga: self.carga,
            Test: false
        };

        return that;
    };

    function addClickFelicidad(item) {
        document.getElementById(item.key).addEventListener('click', function () {
            setFelidad(item.value);
            clearSelecteds('.opciones.felicidad');
            setSelected(item.key);
        });
    };

    function addClickCarga(item) {
        document.getElementById(item.key).addEventListener('click', function () {
            setCarga(item.value);
            clearSelecteds('.opciones.cargabilidad');
            setSelected(item.key);

        });
    };

    function setFelidad(value) {
        self.felicidad = value;
    };

    function setCarga(value) {
        self.carga = value;
    };

    function setSelected(id) {
        document.getElementById(id).className = "back-selected ";
    };

    function clearSelecteds(prefix) {
        var selecteds = document.querySelectorAll(prefix + " button.back-selected");
        if (selecteds.length > 0) {
            selecteds.forEach(function (item) { item.className = ''; });
        }
    };

    return new App();
})()