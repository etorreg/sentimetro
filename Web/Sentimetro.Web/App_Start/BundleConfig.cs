﻿using System.Web;
using System.Web.Optimization;

namespace Sentimetro.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/normalize.css",
                        "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/js/app")
                .Include("~/Scripts/ajaxHelper.js")
                .Include("~/Scripts/app.js")
                );
        }
    }
}
