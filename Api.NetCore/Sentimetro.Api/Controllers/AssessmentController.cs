﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNet.Mvc;
using Sentimetro.Entities;
using Sentimetro.IService;

namespace Sentimetro.Api.Controllers
{
    [Route("Api/[Controller]")]
    public class AssessmentController : Controller
    {
        private IAssessmentService service;

        public AssessmentController(IAssessmentService service)
        {
            this.service = service;
        }


        [HttpGet]
        public async Task<List<Assessment>> GetAll()
        {
            return await service.GetAll();
        }
    }
}
