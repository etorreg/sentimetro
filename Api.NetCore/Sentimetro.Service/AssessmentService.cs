﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sentimetro.IService;
using Sentimetro.Service.Core;
using Sentimetro.Entities;

namespace Sentimetro.Service
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class AssessmentService : CoreService, IAssessmentService
    {
        public async Task<List<Assessment>> GetAll()
        {
            var list = new List<Assessment>();
            return await Task.FromResult(list);
        }
    }
}
